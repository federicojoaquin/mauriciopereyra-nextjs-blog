/** @type {import('next').NextConfig} */
const withImages = require("next-images");

const nextConfig = {
  experimental: {
    appDir: true,
  },
  images: {
    domains: [
      "drive.google.com",
      "i.ibb.co",
      "https://drive.google.com/uc?export=download&id=1zwBxpJioo7WzNfWFhS7QSkguGF-5V7VA",
      "https://i.ibb.co/DDcw7xf/primero.png",
      "https://i.ibb.co/N7MGNpJ/segundo.jpg",
      "https://i.ibb.co/gz2mxrM/tercero.jpg",
      "https://i.ibb.co/NSBJwQQ/cuarto.jpg",
      "https://i.ibb.co/6gdZnfF/quinto.jpg",
      "https://i.ibb.co/zxPm95G/sexto.jpg",
      "https://i.ibb.co/R2vQJLq/Whats-App-Image-2023-09-21-at-08-40-17.jpg",
      "https://i.ibb.co/rGXsj29/Whats-App-Image-2023-09-21-at-08-40-02.jpg",
    ],
    formats: ['image/webp'],
    // disable static imports for image files
    disableStaticImages: false,
  },
  // webpack: (config, options) => {
  //   config.module.rules.push({
  //     test: /\.(png|jpe?g|gif|mp4|jpg)$/i,
  //     use: [
  //       {
  //         loader: 'file-loader',
  //       },
  //     ],
  //   })
  //   return config
  // },
}

module.exports = nextConfig;