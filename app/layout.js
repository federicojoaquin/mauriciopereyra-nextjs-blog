import './globals.css'
import Script from 'next/script'
import Head from 'next/head'
import Navbar from './components/Navbar'

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <head>
        <meta charSet="utf-8" />
        <title>Mauricio Pereyra</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        {/* styles */}
        <link rel="stylesheet" href="assets/css/fancybox/jquery.fancybox.css" />
        <link href="assets/css/bootstrap.css" rel="stylesheet" />
        <link href="assets/css/bootstrap-theme.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/css/slippry.css" />
        <link href="assets/css/style.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/color/default.css" />
        <Script src="assets/js/jquery-1.9.1.min.js"></Script>
        <Script src="assets/js/jquery.easing.js"></Script>
        <Script src="assets/js/classie.js"></Script>
        <Script src="assets/js/bootstrap.js"></Script>
        <Script src="assets/js/slippry.min.js"></Script>
        <Script src="assets/js/nagging-menu.js"></Script>
        <Script src="assets/js/jquery.nav.js"></Script>
        <Script src="assets/js/jquery.scrollTo.js"></Script>
        <Script src="assets/js/jquery.fancybox.pack.js"></Script>
        <Script src="assets/js/jquery.fancybox-media.js"></Script>
        <Script src="assets/js/masonry.pkgd.min.js"></Script>
        <Script src="assets/js/imagesloaded.js"></Script>
        <Script src="assets/js/jquery.nicescroll.min.js"></Script>
        <Script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD8HeI8o-c1NppZA-92oYlXakhDPYR7XMY"></Script>
        <Script src="assets/js/AnimOnScroll.js"></Script> 
      </head>
      <body>
        
        {children}
        </body>
    </html>
  )
}
