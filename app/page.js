"use client"

import React from 'react'
import Script from 'next/script'
import Image from 'next/image'
import { useEffect, useState } from 'react';
import Image1 from '../public/assets/img/slide/1.jpg'
// import sl1 from './Images/primero.PNG'
// import sl2 from './Images/segundo.jpg'
// import sl3 from './Images/tercero.jpg'
// import sl4 from './Images/cuarto.jpg'
// import sl5 from './Images/quinto.jpg'
// import sl6 from './Images/sexto.jpg'
import Styles from './page.module.css'
import SectionOneGray from './components/SectionOneGray.js'
import SectionOne from './components/SectionOne.js'
import Navbar from './components/Navbar.js'
import Spotify from './components/Spotify'
import Link from 'next/link'
import Slideer from './components/Slideer'
import SpotIcon from '../public/assets/icons/spotify.svg'
import '../node_modules/bootstrap-icons/font/bootstrap-icons.css'
import Espectaculos from './components/Espectaculos'
import Albumes from './components/Albumes'


function page() {

  const [windowWidth, setWindowWidth] = useState(0);

  useEffect(() => {
    // Add an event listener to track window width changes
    const handleResize = () => {
      setWindowWidth(window.innerWidth);
    };
    window.addEventListener('resize', handleResize);
    // Initial window width
    setWindowWidth(window.innerWidth);

    // Clean up the event listener when the component unmounts
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  // Check if window width is less than 990px
  const isMobile = windowWidth < 990;


  return (
    <>
    <header>
      <Navbar />
    </header>
  {/* section intro */}
  <section id="intro">
    <ul id="slippry-slider">
      <li>
        <a href="#slide1">
          <Image
            layout='responsive'
            width={100}
            height={100}
            src={Image1}
            alt="Bienvenidos a ProOnliPc Tutoriales!"
            className={`${Styles.image} ${isMobile ? Styles['image-mobile'] : ''}`}
          />
        </a>
      </li>
    </ul>
  </section>
  {/* end intro */}
  {/* Section about */}
  <SectionOne />
  {/* Seccion de intro Mauri */}
  <SectionOneGray />
  
  {/* end section about */}
  {/* section works */}
  
  {/* section works */}
  {/* section contact */}
  {/* <section id="contact" className="section">
    <div className="container">
      <div className="row">
        <div className="col-md-8 col-md-offset-2">
          
        </div>
      </div>
    </div>
    <div className="container">
      <div className="row">
        
      </div>
    </div>
  </section> */}
  <section id="musica" className="section">
    <div className="container">
      <div className="row">
        <div className="col-md-8 col-md-offset-2">
          <div className="heading">
            <h3>
              <span>Música</span>
            </h3>
          </div>
          {/* <div className="sub-heading">
            <p>
              Creating a visual language around the beliefs of the brands we
              work with.
            </p>
          </div> */}
        </div>
      </div>
      <Spotify />
      <div className='row'>
        <a href='https://open.spotify.com/artist/3d10UL0cCMk5mp6cXy9gFG' target='_blank'>
          <div className={Styles.btnCont}>
          
            <div className={Styles.spotifyBtn}>
              <b>Visitar        <i className="bi bi-spotify"/></b>
              
            </div>
          </div>
        </a>
        
      </div>
    
      <div className={Styles.container}>
        <h1 style={{textAlign: 'center'}}>
            <span>Videos</span>
        </h1>
        <iframe src="https://www.youtube.com/embed/IHb-n7p6fp0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen className={Styles.youtube}></iframe>
        <iframe src="https://www.youtube.com/embed/Xf-blweCdjo?start=173" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen className={Styles.youtube}></iframe>
        
      </div>

      {/* <div className={Styles.audioContainer}>
        
      </div> */}
    </div>
  </section>
  {/* end section contact */}
  <Albumes/>
  <Espectaculos />
  <footer id='contact'>
    <div className="verybottom">
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <div className="aligncenter">
              <ul className="social-network social-circle">
                <li>
                  <a href="https://www.instagram.com/mauripereyramusico/" target="_blank" className="icoRss" title="Instagram">
                    <i className="bi bi-instagram" />
                  </a>
                </li>
                <li>
                  <a href="https://www.facebook.com/MauriPereyra28" target="_blank" className="icoFacebook" title="Facebook">
                    <i class="bi bi-facebook"></i>
                  </a>
                </li>
                <li>
                  <a href="https://www.youtube.com/@mauriciopereyra7821" target="_blank" className="icoTwitter" title="Youtube">
                    <i class="bi bi-youtube"></i>
                  </a>
                </li>
                <li>
                  <a href="https://wa.link/0cmhzy" className="icoGoogle" title="Whatsapp">
                    <i class="bi bi-whatsapp"></i>
                  </a>
                </li>
                <li>
                  <a href="https://open.spotify.com/artist/3d10UL0cCMk5mp6cXy9gFG" target="_blank" className="icoLinkedin" title="Spotify">
                    <i className="bi bi-spotify"/>
                  </a>
                </li>
                <li>
                  <a href="https://mail.google.com/mail/u/0/?tab=rm&ogbl#search/maurypereyraguitarra%40gmail.com+sent?compose=new" target="_blank" className="icoGmail" title="Gmail">
                    <i className="bi bi-envelope-paper"/>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <Script src="assets/js/jquery-1.9.1.min.js"></Script>
  <Script src="assets/js/jquery.easing.js"></Script>
  <Script src="assets/js/classie.js"></Script>
  <Script src="assets/js/bootstrap.js"></Script>
  <Script src="assets/js/slippry.min.js"></Script>
  <Script src="assets/js/nagging-menu.js"></Script>
  <Script src="assets/js/jquery.nav.js"></Script>
  <Script src="assets/js/jquery.scrollTo.js"></Script>
  <Script src="assets/js/jquery.fancybox.pack.js"></Script>
  <Script src="assets/js/jquery.fancybox-media.js"></Script>
  <Script src="assets/js/masonry.pkgd.min.js"></Script>
  <Script src="assets/js/imagesloaded.js"></Script>
  <Script src="assets/js/jquery.nicescroll.min.js"></Script>
  <Script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD8HeI8o-c1NppZA-92oYlXakhDPYR7XMY"></Script>
  <Script src="assets/js/AnimOnScroll.js"></Script> 
  <Script src="assets/js/owl.carousel.min.js"></Script>
  {/* <script>
    $(window).on(function(){
      $('.owl-carousel').owlCarousel({
        loop:true,
        margin:5,
        nav:false,
        responsiveClass:true,
        autoplay:true,
        autoplayTimeout:2500,
        responsive:{
          0:{
              items:1,
          },
          600:{
              items:2
          },
          1000:{
              items:4
          }
        }
      })
    })
  </script> */}
  <script src="assets/js/custom.js"></script>
  <script src="contactform/contactform.js"></script> 

</>

  )
}

export default page
