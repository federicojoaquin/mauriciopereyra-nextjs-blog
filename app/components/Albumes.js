"use client"

import React, { useEffect, useState } from 'react';
import Image from 'next/image';

function Albumes() {
  const [imageDimensions, setImageDimensions] = useState({ width: 300, height: 300 });

  const handleResize = () => {
    // Set the default dimensions
    let newDimensions = { width: 450, height: 300 };

    // If the screen width is less than 480 pixels, adjust dimensions
    if (window.innerWidth < 480) {
      newDimensions = { width: 350, height: 200 };
    }

    // Update the state with the new dimensions
    setImageDimensions(newDimensions);
  };

  // Listen for window resize events
  useEffect(() => {
    handleResize(); // Call it initially
    window.addEventListener('resize', handleResize);

    // Clean up the event listener on component unmount
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  return (
    <div className='container' style={{ marginTop: '30px' }}>
      <div className='row' style={{ display: 'flex', justifyContent: 'center' }}>
        <h1>Albumes</h1>
      </div>
      <div className='row'>
        <div className='col-md-6' style={{ marginTop: '60px' }}>
          <div className='row' style={{ display: 'flex', justifyContent: 'center' }}>
            <a href='https://open.spotify.com/intl-es/album/3mB1ywdSDIeLso9LuFMDGT?si=XCOiWOgHTNKKkB2xiAcHMg' target='_blank'>
              <Image
                width={300}
                height={300}
                src='https://i.ibb.co/R2vQJLq/Whats-App-Image-2023-09-21-at-08-40-17.jpg'
              />
            </a>
          </div>
          <h4 className='row text-center' style={{marginTop: '20px'}}>Alas</h4>
        </div>
        <div className='col-md-6' style={{ marginTop: '60px' }}>
          <div className='row' style={{ display: 'flex', justifyContent: 'center' }}>
            <a href='https://open.spotify.com/intl-es/album/4smArPCcqrzAlBSqRV8h0Z?si=W0dTPCQ9SNWS8A11iLhbRQ' target='_blank'>
              <Image
                width={300}
                height={300}
                src='https://i.ibb.co/rGXsj29/Whats-App-Image-2023-09-21-at-08-40-02.jpg'
              />
            </a>    
          </div>
          <h4 className='row text-center' style={{marginTop: '20px'}}>Instantes</h4>
        </div>
        <div className='col-md-12' style={{ marginTop: '60px' }}>
          <div className='row' style={{ display: 'flex', justifyContent: 'center' }}>
            <a href='https://open.spotify.com/intl-es/album/6nfL0OQLBIsvb6yQ3XMqS4?si=_QtZPaiJQu6Ufv7k7c4lhQ' target='_blank'>
              <Image
                width={300}
                height={300}
                src='https://i.ibb.co/fqNrSk9/Portada-ritual-de-luz.png'
              />
            </a>
          </div>
          <h4 className='row text-center' style={{marginTop: '20px'}}>Ritual de luz</h4>
        </div>
      </div>
      <div className='row' style={{ display: 'flex', justifyContent: 'center', flexDirection: 'column' }}>
        <h4 style={{ margin: 'auto' }}>Nominaciones</h4>
        <div className='col-md-12' style={{ marginTop: '60px' }}>
          <div className='row' style={{ display: 'flex', justifyContent: 'center' }}>
            <Image width={imageDimensions.width} height={imageDimensions.height} src='https://i.ibb.co/SvNqgR0/Whats-App-Image-2023-10-06-at-10-53-45.jpg' />
          </div>
        </div>
        <div className='col-md-12' style={{ marginTop: '60px' }}>
          <div className='row' style={{ display: 'flex', justifyContent: 'center' }}>
            <Image width={imageDimensions.width} height={imageDimensions.height} src='https://i.ibb.co/jV8pkRr/Whats-App-Image-2023-10-20-at-23-46-09.jpg"' />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Albumes;

