import React from 'react'
import './SectionOneGray.css'
import Image from 'next/image'

function SectionOneGray() {

  return (
    <section id="about" className="section gray">
        <div className="container">
        <div className="row">
            <div className="col-md-8 col-md-offset-2">
            <div className="heading">
                <h3 className='mainTitle'>
                Mauricio Pereyra
                </h3>
                <p><b>Compositor, arreglador e intérprete de música popular Argentina</b></p>
                <p style={{fontStyle: 'italic'}}>“Cantar nomas porque si</p>
                <p style={{fontStyle: 'italic'}}> Eso lo saben las aves, </p> 
                <p style={{fontStyle: 'italic'}}> Nosotros vamos al aire </p>
                <p style={{fontStyle: 'italic'}}> Con esperanza buscando, </p>
                <p style={{fontStyle: 'italic'}}> Sabedores del bien y del mal</p>
                <p style={{fontStyle: 'italic'}}> Por ahí queremos volar,</p>
                <p style={{fontStyle: 'italic'}}> Y terminamos cantando”.</p>  
                <p style={{fontStyle: 'italic'}}>Churli Corroza</p>
                <Image src={"https://i.ibb.co/M9G8cZf/Mauri-Blanco.jpg"} alt=""
                layout='responsive' height={100} width={300}></Image>
                {/* <Image src={"/assets/img/slide/2.jpg"} alt=""
                layout='responsive' height={100} width={300}></Image> */}
            </div>
            {/* <div className="sub-heading">
                <p>
                We have a history of doing what our name implies, creating a
                visual language around the beliefs of the brands we work with.
                </p>
            </div> */}
            </div>
        </div>
    
        </div>
    </section>
  )
}

export default SectionOneGray
