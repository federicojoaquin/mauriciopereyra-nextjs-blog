// "use client"

// import React, { useEffect, useState } from 'react';
// import Link from 'next/link';

// function Navbar({ anotherPage }) {
//     const [clientWindowHeight, setClientWindowHeight] = useState("");
//     const [backgroundTransparacy, setBackgroundTransparacy] = useState(0);
//     const [padding, setPadding] = useState(30);
  
//     useEffect(() => {
//       window.addEventListener("scroll", handleScroll);
//       return () => window.removeEventListener("scroll", handleScroll);
//     });
  
//     const handleScroll = () => {
//       setClientWindowHeight(window.scrollY);
//     };
  
//     useEffect(() => {
//       let backgroundTransparacyVar = clientWindowHeight / 600;
    
//       if (backgroundTransparacyVar < 1 && screen.width < 990) {
//         let paddingVar = 30 - backgroundTransparacyVar * 20;
//         setBackgroundTransparacy(backgroundTransparacyVar);
//         setPadding(paddingVar);
       
//       }

//       if (screen.width > 990 || anotherPage) {
//         let backgroundTransparacyVar = 1; 
//         let paddingVar = 20;
//         setBackgroundTransparacy(backgroundTransparacyVar); 
//         setPadding(paddingVar)
//       }
  
//     }, [clientWindowHeight]);
  
//     return (
//         <div
//           id="navigation"
//           className="navbar navbar-inverse navbar-fixed-top default"
//           role="navigation"
//           style={{
//             background: `rgba(42, 42, 42, ${backgroundTransparacy})`,
//             padding: `${padding}px 0px`,
//           }}

//         >
//           <div className="container">
//             {/* Brand and toggle get grouped for better mobile display */}
//             <div className="navbar-header">
//               <button
//                 type="button"
//                 className="navbar-toggle"
//                 data-toggle="collapse"
//                 data-target="#bs-example-navbar-collapse-1"
//               >
//                 <span style={{background: `rgba(255, 255, 255, ${backgroundTransparacy}`}} className="icon-bar" />
//                 <span style={{background: `rgba(255, 255, 255, ${backgroundTransparacy}`}} className="icon-bar" />
//                 <span style={{background: `rgba(255, 255, 255, ${backgroundTransparacy}`}} className="icon-bar" />
//               </button>
//               <a className="navbar-brand" href="index.html"
//               style={{color: `rgba(255, 255, 255, ${backgroundTransparacy})`}}>
//                 Mauricio Pereyra
//               </a>
//             </div>
//             <div className="navigation">
//                     <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
//                 <nav>
//                 <ul className="nav navbar-nav navbar-right">
                    
//                     <li>
//                     <a href="#works">Sobre mí</a>
//                     </li>
//                     <li>
//                     <a href="#musica">Musica</a>
//                     </li>
//                     <li>
//                     <a href="#espectaculos">Espectáculos</a>
//                     </li>
//                     <li>
//                     <a href="#contact">Contacto</a>
//                     </li>
//                 </ul>
//                 </nav>
//             </div>
//               {/* /.navbar-collapse */}
//             </div>
//           </div>
//         </div>
      
//     );
//   }
  
// export default Navbar;


"use client"

import React, { useEffect, useState } from 'react';
import Link from 'next/link';

function Navbar({ anotherPage }) {
  const [clientWindowHeight, setClientWindowHeight] = useState("");
  const [backgroundTransparacy, setBackgroundTransparacy] = useState(0);
  const [padding, setPadding] = useState(30);

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  });

  useEffect(() => {
    // Get the initial scroll position
    const initialScrollPosition = window.scrollY;

    if (screen.width > 990) {
      setBackgroundTransparacy(1);
      setPadding(20);
    }

    // Set the initial transparency and padding
    const initialTransparency = 1
    const initialPadding = 20;

    setBackgroundTransparacy(initialTransparency);
    setPadding(initialPadding);

   
  }, []); // Empty dependency array for the initial render

  const handleScroll = () => {
    setClientWindowHeight(window.scrollY);
  };
  
    return (
      <div
        id="navigation"
        className="navbar navbar-inverse navbar-fixed-top default"
        role="navigation"
        style={{
          background: `rgba(42, 42, 42, ${backgroundTransparacy})`,
          padding: `${padding}px 0px`,
        }}
      >
          <div className="container">
            {/* Brand and toggle get grouped for better mobile display */}
            <div className="navbar-header">
              <button
                type="button"
                className="navbar-toggle"
                data-toggle="collapse"
                data-target="#bs-example-navbar-collapse-1"
              >
                <span style={{background: `rgba(255, 255, 255, ${backgroundTransparacy}`}} className="icon-bar" />
                <span style={{background: `rgba(255, 255, 255, ${backgroundTransparacy}`}} className="icon-bar" />
                <span style={{background: `rgba(255, 255, 255, ${backgroundTransparacy}`}} className="icon-bar" />
              </button>
              <a className="navbar-brand" href="index.html"
              style={{color: `rgba(255, 255, 255, ${backgroundTransparacy})`}}>
                Mauricio Pereyra
              </a>
            </div>
            <div className="navigation">
                    <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <nav>
                <ul className="nav navbar-nav navbar-right">
                    
                    <li>
                    <a href="#works">Sobre mí</a>
                    </li>
                    <li>
                    <a href="#musica">Musica</a>
                    </li>
                    <li>
                    <a href="#espectaculos">Espectáculos</a>
                    </li>
                    <li>
                    <a href="#contact">Contacto</a>
                    </li>
                </ul>
                </nav>
            </div>
              {/* /.navbar-collapse */}
            </div>
          </div>
        </div>
      
    );
  }
  
export default Navbar;

