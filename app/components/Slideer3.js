"use client"

import React from 'react';
import { Slide } from 'react-slideshow-image';
import '../../node_modules/react-slideshow-image/dist/styles.css'
import Image from 'next/image';
import './Slideer.css'

const spanStyle = {
  padding: '20px',
  background: '#efefef',
  color: '#000000'
}

const divStyle = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  backgroundSize: 'cover',
  height: '600px'
}
const slideImages = [
  {
    url: 'https://i.ibb.co/Byt6bMT/JoseLuis.jpg',
    caption: 'Slide 10'
  },
  {
    url: 'https://i.ibb.co/bP02TVG/Untitled-design.png',
    caption: 'Slide 1'
  },
  {
    url: 'https://i.ibb.co/mTNfV7v/segundo.jpg',
    caption: 'Slide 2'
  },
  {
    url: 'https://i.ibb.co/q5Bs29g/tercero.jpg',
    caption: 'Slide 3'
  },
  {
    url: 'https://i.ibb.co/DwnNFYJ/Mauricio-3.png',
    caption: 'Slide 4'
  },
  {
    url: 'https://i.ibb.co/pJsbn5K/Mauricio-2.png ',
    caption: 'Slide 5'
  },
  {
    url: 'https://i.ibb.co/3SXw7NF/IMG-20190521-WA0051.jpg',
    caption: 'Slide 6'
  },
  {
    url: 'https://i.ibb.co/2y6thWY/juan-mauri-1280.jpg',
    caption: 'Slide 7'
  },
  {
    url: 'https://i.ibb.co/W0VjBZh/galeria-1280.jpg',
    caption: 'Slide 8'
  },
  {
    url: 'https://i.ibb.co/gVSmDqZ/Gasleria1-1280.jpg',
    caption: 'Slide 9'
  },
];

const Slideshow = () => {
  return (
    <div className='container mTop'>
      <h4 style={{textAlign: 'center'}}>Galería</h4>
      <div className="slide-container">
        <Slide>
        {slideImages.map((slideImage, index)=> (
            <div key={index}>
              
              <Image height={853} width={1280} layout='responsive' src={slideImage.url} alt="slide_image" />
              
            </div>
          ))} 
        </Slide>
      </div>
    </div> 
  )
}

export default Slideshow;