"use client"

import React from 'react';
import { Slide } from 'react-slideshow-image';
import '../../node_modules/react-slideshow-image/dist/styles.css'
import Image from 'next/image';

const spanStyle = {
  padding: '20px',
  background: '#efefef',
  color: '#000000'
}

const divStyle = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  backgroundSize: 'cover',
  height: '600px'
}
const slideImages = [
  {
    url: 'https://i.ibb.co/3SXw7NF/IMG-20190521-WA0051.jpg',
    caption: 'Slide 1'
  },
  {
    url: 'https://i.ibb.co/mTNfV7v/segundo.jpg',
    caption: 'Slide 2'
  },
  {
    url: 'https://i.ibb.co/q5Bs29g/tercero.jpg',
    caption: 'Slide 3'
  },
  {
    url: 'https://i.ibb.co/PwwvkQd/cuarto.jpg',
    caption: 'Slide 3'
  },
];

const Slideshow = () => {
    return (
      <div className="slide-container">
        <Slide>
         {slideImages.map((slideImage, index)=> (
            <div key={index}>
              
              <Image height={853} width={1280} layout='responsive' src={slideImage.url} alt="slide_image" />
              
            </div>
          ))} 
        </Slide>
      </div>
    )
}

export default Slideshow;