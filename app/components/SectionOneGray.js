"use client"

import React, { useRef, useEffect, useState } from 'react'
import Styles from '../page.module.css'
import './SectionOneGray.css'

function SectionOneGray() {

    const ref = useRef(null);
  
    const myRef = useRef(); 
    const [myElementIsVisible, setMyElementIsVisible] = useState(); 
  
    useEffect(() => {
    
        const observer = new IntersectionObserver((entries) => {
        const entry = entries[0]; 
        setMyElementIsVisible(entry.isIntersecting)
        })
        observer.observe(myRef.current); 
   
    }, [])

  return (
    <section id="works" className="section gray">
        <div className="container">
        <div className="row">
            <div className="col-md-8 col-md-offset-2">
            <div className="heading">
                <h3>
                <span>Biografía</span>
                </h3>
            </div>
            {/* <div className="sub-heading">
                <p>
                We have a history of doing what our name implies, creating a
                visual language around the beliefs of the brands we work with.
                </p>
            </div> */}
            </div>
        </div>
        
        <div ref={myRef} className={`row cont ${myElementIsVisible ? 'vis izq' : 'notVis'}`}>
            <div className='col-md-12'>
            <p>Compositor, cantante, interprete y guitarrista de la Ciudad de Córdoba, Argentina. Nació en la ciudad de Villa Dolores (Provincia de Córdoba) el 28 de febrero de 1983. Se inició en la música a muy temprana edad, hijo de Delfín Pereyra reconocido compositor del Valle de Traslasierra. Inició sus estudios y dió sus primeros pasos en los escenarios en su Villa Dolores natal. Años más tarde se radicó en Córdoba Capital, donde estudió Licenciatura en Composición musical en la Universidad Nacional de Córdoba, Profesorado Superior de Artes en Música en el Conservatorio Félix T. Garzón y  guitarra con distintos maestros, tales como Horacio Burgos, Pablo De Giusto y Carolina Velázquez, entre otros.  Además ha realizado cursos con distintos referentes a nivel nacional como Roberto Calvo y María del Carmen Aguilar. Realizó composiciones conjuntamente con  Jorge Marziali, José Luis Aguirre, Gustavo "Negro" Vergara, Leandro Manuel Calle, Chaly Sabas, Fernando G. Toledo, Carlos Garro Aguilar y Aníbal Albornoz Ávila, entre otros.
            Lleva editados tres trabajos discográficos: <b>“Alas”</b> (2014), <b>“Instantes”</b> (2021), disco que recibió tres
            nominaciones para los <b>“Premios Mercedes Sosa”</b>, como mejor Álbum Folclore Alternativo, mejor
            Álbum solista de Folclore y mejor Videoclip, y su último disco <b>“Ritual de Luz”</b> (2024); estos tres
            trabajos discográficos están compuestos por composiciones musicales propias en su totalidad, con
            la participación de artistas invitados como: José Luis Aguirre, José Luis Serrano (Doña Jovita),
            Mario Díaz, Jesús Hidalgo, Rodrigo Carazo, Marita Londra, Eli Fernández, Miguel Rivaynera y Diego
            Cortez entre otros.
            En el año 2023 ganó la Beca Creación del Fondo Nacional de las Artes.
            Ha participado como arreglador y artista invitado en discos de colegas como José Luis Aguirre,
            Gustavo Vergara, Andrés Muratore y Emanuel Orona entre otros.
            Formó parte de varios espectáculos como “La Vida dos Veces” (homenaje A Hamlet Lima
            Quintana y Armando Tejada Gómez), “Una mujer llamada Pablo” (homenaje a “Nenette”, quien
            fuera compañera de Atahualpa Yupanqui y autora de grandes clásicos de nuestra música popular
            Argentina), y “La Tierra de Uno”, (tributo a Manuel Castilla, poeta salteño, figura imprescindible de
            nuestro cancionero folclórico.</p>
                
            </div> 
        </div>
        <div className='row'>
            <a href='/pdfs/Rider de Sonido mauri.pdf' id="enlaceDescargarPdf" download="Rider de Sonido mauri.pdf">
            <div className={Styles.btnCont}>
            
                <div className={Styles.riderBtn}>
                <b>Rider sonido                 <i className="bi bi-download"/></b>
                
                </div>
            </div>
            </a>
        
        </div> 
        
            
        
        </div>
    </section>
  )
}

export default SectionOneGray
