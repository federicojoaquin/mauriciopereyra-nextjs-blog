"use client"

import React, { useRef, useEffect, useState } from 'react'
import './Spotify.css'

function Spotify() {
    const ref = useRef(null);
  
    const myRef = useRef(); 
    const [myElementIsVisible, setMyElementIsVisible] = useState(); 
  
    useEffect(() => {
    
        const observer = new IntersectionObserver((entries) => {
        const entry = entries[0]; 
        setMyElementIsVisible(entry.isIntersecting)
        })
        observer.observe(myRef.current); 
   
    }, [])

  return (
    <div className={`row cont ${myElementIsVisible ? 'vis izq' : 'notVis'}`} ref={myRef} 
        style={{
            display: 'flex',
            justifyContent: 'center',
            gap: '20px', /* Espaciado entre columnas */
            flexWrap: 'wrap', /* Para manejar responsividad si es necesario */
        }}
    >
        <div className="col-md-3">
            <h2 style={{fontSize: '2rem', textAlign: 'center'}}>Los Chuncanitos del Río</h2>
            <iframe style={{borderRadius: '12px'}}  
                    src="https://open.spotify.com/embed/track/648JNYNidCd19zZhMawMOH?utm_source=generator" 
                    frameBorder="0" width="100%" height="200" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; 
                    fullscreen; picture-in-picture" loading="lazy"></iframe>
        </div>
        <div className="col-md-3">
            <h2 style={{fontSize: '2rem', textAlign: 'center'}}>Parral de la amistad</h2>
            <iframe style={{borderRadius: '12px'}}  
                src="https://open.spotify.com/embed/track/27VKtxAURPGg3n06q5ZHDn?utm_source=generator" width="100%" height="200" 
                frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media;
                fullscreen; picture-in-picture" loading="lazy">
            </iframe>
        </div>
        
        <div className="col-md-3">
            <h2 style={{fontSize: '2rem', textAlign: 'center'}}>Barrilete</h2>
                <iframe style={{borderRadius: '12px'}}  
                src="https://open.spotify.com/embed/track/6niluhv4pFEOikkXy07Cqt?utm_source=generator" 
                width="100%" height="200" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media;
                fullscreen; picture-in-picture" loading="lazy">     
                </iframe>   
        </div>
        
   </div>
  )
}

export default Spotify
