import React from 'react'
import './SectionOneGray.css'
import Image from 'next/image'
import Slideer from './Slideer.js'
import Slideer2 from './Slideer2.js'
import Slideer3 from './Slideer3.js'
import OwlCarrousel from './OwlCarrousel'

function Espectaculos() {

  return (
    <section id="espectaculos" className="section">
        <div className="container">
            <h1 className="heading">Otros espectáculos</h1>
            <div className="row">
                <Slideer />
                <Slideer2  />
                <Slideer3 />                 
            </div>
            
        </div>
    </section>
  )
}

export default Espectaculos;
