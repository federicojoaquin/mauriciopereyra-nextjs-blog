"use client"

import React from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import './Slideer.css'
import Styles from '../page.module.css'
import 'swiper/css';
import 'swiper/css/effect-coverflow';
import 'swiper/css/pagination';
import 'swiper/css/navigation';
import Image from 'next/image';
import './Slideer.css'
 
import { EffectCoverflow, Pagination, Navigation } from 'swiper';

// import slide_image_1 from '../../public/assets/img/slide/1.jpg';
// import slide_image_2 from '../../public/assets/img/slide/1.jpg';
// import slide_image_3 from '../../public/assets/img/slide/1.jpg';
// import slide_image_4 from '../../public/assets/img/slide/1.jpg';
// import slide_image_5 from '../../public/assets/img/slide/1.jpg';
// import slide_image_6 from '../../public/assets/img/slide/1.jpg';
// import slide_image_7 from '../../public/assets/img/slide/1.jpg';

function App() {
  return (
    <div className="container mTop">
      <h4 style={{textAlign: 'center'}}>La vida dos veces</h4>
      <div className={Styles.container} style={{marginBottom: '4rem'}}>
        <iframe src="https://www.youtube.com/embed/ad7Lgs7SDPw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen className={Styles.youtube}></iframe>
        <iframe src="https://www.youtube.com/embed/n-0i_g9xmLY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen className={Styles.youtube}></iframe>
        <iframe src="https://www.youtube.com/embed/XSB9ymcAAhc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen className={Styles.youtube}></iframe>
        
      </div>
      <Swiper
        effect={'coverflow'}
        grabCursor={true}
        centeredSlides={true}
        loop={true}
        slidesPerView={'auto'}
        coverflowEffect={{
          rotate: 0,
          stretch: 0,
          depth: 100,
          modifier: 2.5,
        }}
        pagination={{ el: '.swiper-pagination', clickable: true }}
        navigation={{
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
          clickable: true,
        }}
        modules={[EffectCoverflow, Pagination, Navigation]}
        className="swiperContainer"
      >
        <SwiperSlide>
          <Image height={600} width={400} layout='responsive' src="https://i.ibb.co/PwwvkQd/cuarto.jpg" alt="slide_image" />
        </SwiperSlide>
        {/* <SwiperSlide>
          <Image height={600} width={400} layout='responsive' src="https://i.ibb.co/PMqhWY1/IMG-20190521-WA0051.jpg" alt="slide_image1" />
        </SwiperSlide>
        <SwiperSlide>
          <Image height={600} width={400} layout='responsive' src="https://i.ibb.co/PMqhWY1/IMG-20190521-WA0051.jpg" alt="slide_image1" />
        </SwiperSlide>
        <SwiperSlide>
          <Image height={600} width={400} layout='responsive' src="https://i.ibb.co/PMqhWY1/IMG-20190521-WA0051.jpg" alt="slide_image1" />
        </SwiperSlide>
        <SwiperSlide>
          <Image height={600} width={400} layout='responsive' src="https://i.ibb.co/PMqhWY1/IMG-20190521-WA0051.jpg" alt="slide_image1" />
        </SwiperSlide> */}

        <div className="slider-controler">
          <div className="swiper-button-prev slider-arrow">
            <ion-icon name="arrow-back-outline"></ion-icon>
          </div>
          <div className="swiper-button-next slider-arrow">
            <ion-icon name="arrow-forward-outline"></ion-icon>
          </div>
          <div className="swiper-pagination"></div>
        </div>
      </Swiper>
    </div>
  );
}

export default App;