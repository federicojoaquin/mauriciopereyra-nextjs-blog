"use client"

import React from 'react';
import { Slide } from 'react-slideshow-image';
import '../../node_modules/react-slideshow-image/dist/styles.css'
import Image from 'next/image';
import './Slideer.css'

const spanStyle = {
  padding: '20px',
  background: '#efefef',
  color: '#000000'
}

const divStyle = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  backgroundSize: 'cover',
  height: '200px'
}
const slideImages = [
    {
        url: 'https://i.ibb.co/SvNqgR0/Whats-App-Image-2023-10-06-at-10-53-45.jpg',
        caption: 'Slide 2'
    },
    {
        url: 'https://i.ibb.co/ZH4Ft5P/Whats-App-Image-2023-09-21-at-08-41-50.jpg',
        caption: 'Slide 1'
    },
];

const Slideshow = () => {
    return (
      <div className='container mTop' >
        <div className="slide-container" >
          <h4 style={{textAlign: 'center'}}>Nominaciones (instantes)</h4>
          <Slide>
          {slideImages.map((slideImage, index)=> (
              <div key={index} style={{display: 'flex', justifyContent: 'center', marginTop: '30px'}}>
                
                <Image height={300} width={400} src={slideImage.url} alt="slide_image" />
                
              </div>
            ))} 
          </Slide>
        </div>
      </div> 
    )
}

export default Slideshow;